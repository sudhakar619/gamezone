var baseUrl = `https://newsapi.org/v2`;
var apiKey= `aeca3d1f699c4773aa4d4df829b4117f`;
var demo="";
var id1;
var color;
document.getElementById('search').addEventListener('click', function(){
    secondsearch()
});
 function secondsearch() {
  const searchData1 = document.forms.myForm['search-data'].value;
  const arr = [];
  if (searchData1 === '') {
    alert('please enter the data before search');
  } else if(searchData1 !== '' || arr == "") {
      fetch(`${baseUrl}/everything?q=${searchData1}&apiKey=${apiKey}`)
        .then(res => res.json())
        .then((data) => {
          if (data && data.articles && (data.articles == '' || !data.articles.length)){
            let head = document.getElementById('first');
            let block = document.createElement('div');
            block.innerHTML = `<center><h3>NO data is present on this search of <b>${searchData1}</b></h3></center>`;
            // head.appendChild(block);
            head.insertBefore(block, head.firstChild);
          } else if (data.status == 'error') {
            const head = document.getElementById('first');
            const block = document.createElement('div');
            block.innerHTML = 'NO data is present on this search';
            head.appendChild(block);
          } else {
            var demo = data;
           display(demo); 
          }
          console.log(data);
        }).catch((e) => {
          console.log(e);
        });
  }
 }

(function(){
  loading()
  clearfun();
  fetchData(`${baseUrl}/top-headlines?country=in&category=sports&apiKey=${apiKey}`);
})()

function loading(){
  let head = document.getElementById('first');
  let block = document.createElement('div');
  block.innerHTML = `<center><h3>Loading .............</center>`;
  // head.appendChild(block);
  head.insertBefore(block, head.firstChild);
}

function fetchData(url) {
  fetch(url)
  .then(res => res.json()).then((data) => {
    demo = data;
    display(demo);
  }).catch((e) => {
    console.log(e);
  });
}

function clearfun(){
  if(document.forms.myForm['search-data'].value != ""){
    document.forms.myForm['search-data'].value="";
  }
}

function display(data){
  const first = document.getElementById('first');
  first.innerHTML = '';
console.log(data)
  for (let j = 0; j < data.totalResults; j += 1) {
    let dataValue=data.articles[j]
    console.log(dataValue)
    if ( dataValue && !dataValue.urlToImage){
      console.log(dataValue.urlToImage);
    } else if(dataValue) {
      let markup = `
      <div class="rownews" >
      <div class="col">
      <img src="${dataValue.urlToImage}" id="head-img" width="230px" height="180px" >
      </div>
      <div class="col">
    
      <a href="${dataValue.url}" target="blank"><h3>${dataValue.title}</h3> </a>
         <p id="head-des">${dataValue.description}</p>
    </div>
  </div>`;
      let block = document.createElement('div');
      block.setAttribute('id', 'head-news');
      block.innerHTML = markup;
      let head = document.getElementById('first');
      head.appendChild(block);
    }
  }
}



